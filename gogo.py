#search, find_neighburs, bits at position,
#search_children, array append, decode, encode,
B = [0x5555, 0x3333, 0x0f0f]
S = [1, 2, 4]

grid_size = 8
max_level = int(log2(grid_size * 1.0))

def split_binary(x):
    nx = (x | (x <<S[2])) & B[2]
    nx = (nx | (x << S[1])) & B[1]
    nx = (nx | (x << S[0])) & B[0]
    return nx

def x_coord_from_morton(key):
    x = key & B[0]
    x = (x ^ (x >> S[0])) & B[1]
    x = (x ^ (x >> S[1])) & B[2]
    x = (x ^ (x >> S[2]))
    return x

def y_coord_from_morton(key):
    y = key >> 1
    y = y & B[0]
    y = (y ^ (y >> S[0])) & B[1]
    y = (y ^ (y >> S[1])) & B[2]
    y = (y ^ (y >> S[2]))
    return y


def decode_morton(key):
    return [x_coord_from_morton(key), y_coord_from_morton(key)]


def encode_coord(x,y):
    return split_binary(x) | (split_binary (y) << 1)


def left(key):
    return (((key & 0x5555) - 1) & 0x5555) | (key & 0xAAAA)


def right(key):
    return (((key | 0xAAAA) + 1) & 0x5555) | (key & 0xAAAA)


def top(key):
    return (((key & 0xAAAA) - 1) & 0xAAAA) | (key & 0x5555)


def bot(key):
    return (((key | 0x5555) + 1) & 0xAAAA) | (key & 0x5555)


def bap(key, level):
    return (key >> 2 * (max_level - level)) & 0x03
